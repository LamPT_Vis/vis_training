--cau 1 chuong 2
--select all from table salgrade
SELECT
    *
FROM
    salgrade;

--cau 2 chuong 2
--select all from table EMP
SELECT
    *
FROM
    emp;

--Cau 3 chuong 2
--select all job
SELECT DISTINCT
    emp.job
FROM
    emp;

--cau 4 chuong 2
--select all employee and total sal in a year
SELECT
    emp.ename,
    emp.sal * 12 renumeration
FROM
    emp
ORDER BY
    renumeration DESC;

--cau 5 chuong 2
--select to statement of employee job
SELECT
    emp.ename
    || ' has held the position of '
    || emp.job
    || ' in department '
    || emp.deptno
    || ' from '
    || emp.hiredate
FROM
    emp;

--cau 6 chuong 2
--show structure of table emp
DESC emp;

--cau 7 chuong 2
--change title and format of columns sal and hiredate
COLUMN emp.hiredate HEADING startdate FORMAT a6;

COLUMN emp.sal HEADING salary FORMAT $9999;
--check heading and format
COLUMN emp.hiredate;
COLUMN emp.sal;


--CHUONG 3

--bai 1 chuong 3
--select all employee with sal from 1000 to 2000
SELECT emp.ename, emp.deptno, emp.sal FROM emp WHERE sal BETWEEN 1000 AND 2000;

--bai 2 chuong 3
--select deparment id and name, arrange by name
SELECT dept.deptno, dept.dname FROM dept ORDER BY dname;

--bai 3 chuong 3
--select all employee work in dept 10 and 20
SELECT
    *
FROM emp WHERE deptno IN (10,20);

--bai 4 chuong 3
--show name and job of employees work as clerk in dept 20
SELECT
    emp.ename,
    emp.job
FROM
    emp
WHERE
    emp.job LIKE 'CLERK'
    AND deptno = 20;

--bai 5 chuong 3
--show name of employees which have TH and LL in these
SELECT
    emp.ename
FROM
    emp
WHERE
    ename LIKE '%TH%'
    OR ename LIKE '%LL%';

--bai 6 chuong 3
--show all employees who has president as manager (direct and indirect)
SELECT emp.ename, emp.job, emp.sal FROM emp WHERE mrg = (
    SELECT
        empno
    FROM
        emp
    WHERE
        job LIKE 'PRESIDENT'
) OR mrg IN (
    SELECT
        empno
    FROM
        emp
    WHERE
        mrg = (
            SELECT
                empno
            FROM
                emp
            WHERE
                job LIKE 'PRESIDENT'
        )
) OR mrg
IN

( SELECT
    empno
FROM
    emp
WHERE
    mrg = (
        SELECT
            empno
        FROM
            emp
        WHERE
            job LIKE 'PRESIDENT'
    )
    OR mrg IN (
        SELECT
            empno
        FROM
            emp
        WHERE
            mrg = (
                SELECT
                    empno
                FROM
                    emp
                WHERE
                    job LIKE 'PRESIDENT'
            )
    )
);
    
--bai 7 chuong 3
--show name , deptno, hiredate of who are hired in 1983
SELECT
    emp.ename,
    emp.deptno,
    emp.hiredate
FROM
    emp
WHERE
        hiredate >= TO_DATE('01-JAN-83', 'DD-MON-RR')
    AND hiredate <= TO_DATE('31-DEC-83', 'DD-MON-RR');

--bai 8 chuong 3
--show employees name, anual salary, committion where sal<comm and job is salesman
SELECT
    emp.ename,
    emp.sal * 12 AS anual_sal,
    emp.comm
FROM
    emp
WHERE
        sal > comm
    AND job = 'SALESMAN';

--CHUONG 4

--bai 1 chuong 4
--show name and sal with sal up 15%
SELECT
    emp.ename,
    emp.sal * 1.15 AS pctsal
FROM
    emp;

--bai 2 chuong 4
--x
SELECT
    ( rpad(ename, 10, '*')
      || lpad(job, 10, '*') ) AS name_and_job
FROM
    emp;

--bai 3 chuong 4
--x
SELECT
    ( ename
      || '('
         || ( nls_initcap(job) )
            || ')' ) AS employee
FROM
    emp;

--bai 4 chuong 4
--x
SELECT
    ename,
    deptno,
    nls_initcap(replace(job, 'SALESMAN', 'SALEPERSON'))
FROM
    emp
WHERE
    deptno = 30;

--bai 5 chuong 4
--x
SELECT
    to_char(next_day(add_months(current_date, 2), 'FRIDAY'), 'dd Month yyyy')
FROM
    emp
FETCH NEXT 1 ROWS ONLY;

--bai 6 cuong 4
--x
SELECT
    ename,
    to_char(hiredate, 'month,DDSPTH rrrr') AS hire_date
FROM
    emp
WHERE
    deptno = 20;

--bai 7 chuong 4
--x
SELECT
    ename,
    to_char(hiredate, 'dd-mm-rrrr')                          AS hire_date,
    to_char(add_months(hiredate, 12), 'dd-mm-rrrr')          AS review
FROM
    emp
ORDER BY
    hiredate;

--bai 8 chuong 4
--x
SELECT
    ename,
    decode(sign(sal - 1500), - 1, 'below 1500', 0, 'on target',
           sal) AS salary
FROM
    emp;

--bai 9 chuong 4
--x
SELECT
    to_char(current_date, 'DAY')
FROM
    emp
FETCH NEXT 1 ROWS ONLY;

--bai 10 chuong 4
--x
--SELECT REGEXP_LIKE('12/12','^(..)/(..)$') as IS_VALID FROM emp FETCH NEXT 1  ROWS ONLY;

--bai 11 chuong 4
--x
SELECT
    ename,
    hiredate,
    next_day((
        SELECT
            MAX(hiredate)
        FROM
            emp
    ) + 15, 'friday')
FROM
    emp
GROUP BY
    hiredate,
    ename
ORDER BY
    hiredate;

--bai 1 phan 2 chuong 4
--x
SELECT
    AVG(sal) AS avg_sal
FROM
    emp;

SELECT
    MAX(sal) AS max_sal
FROM
    emp;

SELECT
    MIN(sal) AS min_sal
FROM
    emp;

--bai 2 phan 2 chuong 4
--x
SELECT
    MAX(sal) AS max_sal_per_job,
    job
FROM
    emp
GROUP BY
    job; 

--bai 3 phan 2 chuong 4
--x
SELECT
    COUNT(job)
FROM
    emp
WHERE
    job = 'MANAGER';

--bai 4 phan 2 chuong 4
--x
SELECT DISTINCT
    deptno
FROM
    emp e1
WHERE
    (
        SELECT
            COUNT(empno)
        FROM
            emp e2
        WHERE
            e2.deptno = e1.deptno
    ) > 3;

--bai 5 phan 2 chuong 4
--x
SELECT
    e.mrg            AS manager_id,
    MIN(e.sal)       AS min_sal
FROM
    emp e
WHERE
    e.mrg IN (
        SELECT
            empno
        FROM
            emp
        WHERE
            job = 'MANAGER'
    )
GROUP BY
    e.mrg
ORDER BY
    min_sal;


--CHUONG 5

--bai 1 chuong 5
--Hi?n th? to�n b? t�n nh�n vi�n v� t�n ph�ng ban l�m vi?c s?p x?p theo t�n ph�ng ban.
SELECT
    e.ename,
    d.dname
FROM
    emp   e,
    dept  d
WHERE
    e.deptno = d.deptno
ORDER BY
    e.deptno;

--bai 2 chuong 5
--Hi?n th? t�n nh�n vi�n, v? tr� ??a l�, t�n ph�ng v?i ?i?u ki?n l??ng >1500.
SELECT
    e.ename,
    d.dname,
    d.loc
FROM
    emp   e,
    dept  d
WHERE
        e.deptno = d.deptno
    AND e.sal > 1500;

--bai 3 chuong 5
--Hi?n th? t�n nh�n vi�n, ngh? nghi?p, l??ng v� m?c l??ng.
SELECT
    e.ename,
    e.sal,
    (
        SELECT
            s1.grade
        FROM
            salgrade s1
        WHERE
            e.sal BETWEEN s1.losal AND s1.hisal
    ) AS grade
FROM
    emp e;

--bai 4 chuong 5
--Hi?n th? t�n nh�n vi�n, ngh? nghi?p, l??ng v� m?c l??ng, v?i ?i?u ki?n m?c l??ng=3.
SELECT
    e.ename,
    e.sal,
    e.job,
    s.grade
FROM
    emp       e,
    salgrade  s
WHERE
        s.grade = 3
    AND ( e.sal BETWEEN (
        SELECT
            s1.losal
        FROM
            salgrade s1
        WHERE
            s1.grade = 3
    ) AND (
        SELECT
            s2.hisal
        FROM
            salgrade s2
        WHERE
            s2.grade = 3
    ) );

--bai 5 chuong 5
--Hi?n th? nh?ng nh�n vi�n t?i DALLAS
SELECT
    e.ename,
    d.loc,
    e.sal
FROM
    emp   e,
    dept  d
WHERE
        e.deptno = d.deptno
    AND d.loc = 'DALLAS';

--Bai 6 chuong 5
--Hi?n th? t�n nh�n vi�n , ngh? nghi?p, l??ng, m?c l??ng, t�n ph�ng l�m vi?c tr? nh�n
--vi�n c� ngh? l� cleck v� s?p x?p theo chi?u gi?m.

SELECT
    *
FROM
         (
        SELECT
            e.ename,
            e.job,
            e.sal,
            (
                SELECT
                    s1.grade
                FROM
                    salgrade s1
                WHERE
                    e.sal BETWEEN s1.losal AND s1.hisal
            ) AS grade
        FROM
            emp e
        WHERE
            e.job != 'CLERK'
    )
    NATURAL JOIN (
        SELECT
            e2.ename,
            d2.dname
        FROM
            emp   e2,
            dept  d2
        WHERE
            e2.deptno = d2.deptno
    )
ORDER BY
    sal DESC;

--bai 7 chuong 5
--Hi?n th? chi ti?t v? nh?ng nh�n vi�n ki?m ???c 36000 $ 1 n?m ho?c ngh? l� cleck.
SELECT
    *
FROM
         (
        SELECT
            e.ename,
            e.job,
            e.sal * 12      AS anual_sal,
            (
                SELECT
                    s1.grade
                FROM
                    salgrade s1
                WHERE
                    e.sal BETWEEN s1.losal AND s1.hisal
            )             AS grade
        FROM
            emp e
        WHERE
            e.sal * 12 = 36000
            OR e.job = 'CLERK'
    )
    NATURAL JOIN (
        SELECT
            e2.ename,
            d2.dname
        FROM
            emp   e2,
            dept  d2
        WHERE
            e2.deptno = d2.deptno
    );


--bai 8 chuong 5
--Hi?n th? nh?ng ph�ng kh�ng c� nh�n vi�n n�o l�m vi?c.
SELECT
    *
FROM
    dept d
WHERE
    d.deptno = (
        SELECT
            d1.deptno
        FROM
            emp   e
            FULL OUTER JOIN dept  d1 ON ( e.deptno = d1.deptno )
        WHERE
            ename IS NULL
    );

--bai 9 chuong 5
--Hi?n th? m� nh�n vi�n, t�n nh�n vi�n, m� ng??i qu?n l�, t�n ng??i qu?n l�
SELECT
    e.empno    emp_id,
    e.ename    emp_name,
    m.empno    manager_id,
    m.ename    mgr_name
FROM
    emp  e,
    emp  m
WHERE
        e.mrg = m.empno;
        
--bai 10 chuong 5
--Nh? c�u 9 hi?n th? th�m th�ng tin v? �ng KING.
SELECT
    emp_id,
    emp_name,
    mng_id,
    mng_name
FROM
    (
        SELECT
            e.empno    emp_id,
            e.ename    emp_name,
            e.mrg
        FROM
            emp e
    )  a
    LEFT OUTER JOIN (
        SELECT
            m.empno    mng_id,
            m.ename    mng_name
        FROM
            emp m
    )  b ON a.mrg = b.mng_id;
    
    
--bai 11 chuuong 5
--Hi?n th? ngh? nghi?p ???c tuy?n d?ng v�o n?m 1981 v� kh�ng ???c tuy?n d?ng v�o n?m 1982.
SELECT DISTINCT
    e.job
FROM
    emp e
WHERE
    e.job = ANY (
        SELECT
            e1.job
        FROM
            emp e1
        WHERE
            to_char(hiredate, 'rrrr') = '1981'
    )
    AND e.job != ALL (
        SELECT
            e2.job
        FROM
            emp e2
        WHERE
            to_char(hiredate, 'rrrr') = '1982'
    );

--bai 12 chuong 5
--c
SELECT e.ename as EMP_NAME, e.sal as EMP_SAL ,ee.ename as MGR_NAME, ee.sal as MGR_SAL from emp e, emp ee where ee.empno = e.mgr and ee.HIREDATE  >= e.hiredate;

--bai 13 chuong 5
--x
SELECT
    e.ename        AS emp_name,
    e.hiredate     AS emp_hiredate,
    ee.ename       AS mgr_name,
    ee.hiredate    AS mgr_hiredate
FROM
    emp  e,
    emp  ee
WHERE
    ee.empno = e.mrg;
    
--bai 14 chuong 5
--T�m nh?ng nh�n vi�n ki?m ???c l??ng cao nh?t trong m?i lo?i ngh? nghi?p.
SELECT
    e.job,
    MAX(e.sal)
FROM
    emp e
GROUP BY
    e.job
ORDER BY
    e.job;
-- bai 15 chuong 5
SELECT
    ename,
    job,
    deptno,
    sal
FROM
    emp
WHERE
    sal IN (
        SELECT
            MAX(sal)
        FROM
            emp
        GROUP BY
            deptno
    )
ORDER BY
    deptno ASC;

--bai 16 chuong 5
SELECT
    ename,
    hiredate,
    deptno
FROM
    emp
WHERE
    hiredate IN (
        SELECT
            MIN(hiredate)
        FROM
            emp
        GROUP BY
            deptno
    )
ORDER BY
    deptno;
    
--bai 17 chuong 5
--x
SELECT
    emp.empno,
    emp.ename,
    emp.sal,
    emp.deptno
FROM
         emp
    INNER JOIN (
        SELECT
            deptno,
            AVG(sal) AS avgsal
        FROM
            emp
        GROUP BY
            deptno
    ) tblavgsal ON emp.deptno = tblavgsal.deptno
WHERE
    emp.sal > tblavgsal.avgsal
ORDER BY
    emp.deptno;


--bai 18 chuong 5
--x
SELECT
    m.empno     AS empno,
    m.ename     AS emp_name,
    m.sal       AS emp_sal,
    m.deptno    AS mrg_number,
    e.ename     AS mrg_name,
    e.deptno    AS mrg_dept,
    s.grade     AS mgr_grade
FROM
    emp       e,
    emp       m,
    salgrade  s
WHERE
        e.empno = m.mrg
    AND e.sal BETWEEN s.losal AND s.hisal
    AND e.job != 'PRESIDENT';



--chuong 7 

--bai 1 chuong 7
--x
CREATE TABLE project_lam (
    projid         NUMBER(4) NOT NULL PRIMARY KEY,
    p_desc         VARCHAR2(20),
    p_start_date   DATE,
    p_end_date     DATE,
    budget_amount  NUMBER(7, 2),
    max_no_staff   NUMBER(2),
    CHECK ( p_end_date > p_start_date )
)
--bai 2 chuong 7
--x
CREATE TABLE assignment_lam (
    projid        NUMBER(4) NOT NULL
        CONSTRAINT fk_projid_lam
            REFERENCES project_lam ( projid ),
    empno         NUMBER(4) NOT NULL
        CONSTRAINT fk_empno_lam
            REFERENCES emp ( empno ),
    a_start_date  DATE,
    a_end_date    DATE,
    bill_amount   NUMBER(4, 2),
    assign_type   VARCHAR2(2)
)

--bai 3 chuong 7
--x
ALTER TABLE project_lam ADD (
    comments LONG
);

ALTER TABLE assignment_lam ADD (
    hours NUMBER
);

--bai 4 chuong 7
--x
DESCRIBE user_objects;

--bai 5 chuong 7
--x
ALTER TABLE assignment_lam MODIFY (
    projid unique,
    empno unique
);

--bai 6 chuong 7
--x
DESCRIBE user_constraints;

--bai 7 chuong 7
--z
SELECT
    table_name
FROM
    user_tables;




--chuong 8


--bai 1 chuong 8
--x
INSERT INTO project_lam (
    projid,
    p_desc,
    p_start_date,
    p_end_date,
    budget_amount,
    max_no_staff,
    comments
) VALUES (
    1000,
    'avee player',
    TO_DATE('11-10-2019', 'dd-mm-yyyy'),
    TO_DATE('20-10-2019', 'dd-mm-yyyy'),
    20000.99,
    1,
    234567800
);

INSERT INTO project_lam (
    projid,
    p_desc,
    p_start_date,
    p_end_date,
    budget_amount,
    max_no_staff,
    comments
) VALUES (
    1001,
    'tank 98',
    TO_DATE('15-08-2019', 'dd-mm-yyyy'),
    TO_DATE('10-09-2019', 'dd-mm-yyyy'),
    20000.99,
    1,
    234567800
);

--bai 2 chuong 8
--x
INSERT INTO assignment_lam (
    projid,
    empno,
    a_start_date,
    a_end_date,
    bill_amount,
    assign_type,
    hours
) VALUES (
    1000,
    7698,
    TO_DATE('01-JAN-88', 'DD-MON-RR'),
    TO_DATE('03-JAN-88', 'DD-MON-RR'),
    50,
    'WR',
    15
);

INSERT INTO assignment_lam (
    projid,
    empno,
    a_start_date,
    a_end_date,
    bill_amount,
    assign_type,
    hours
) VALUES (
    1001,
    7782,
    TO_DATE('02-JAN-88', 'DD-MON-RR'),
    TO_DATE('06-JAN-88', 'DD-MON-RR'),
    50,
    'WR',
    15
);


--bai 3 chuong 8
--x
update assignment_lam set ASIGN_TYPE='wr'
where ASIGN_TYPE='wt';



--chuong 9

--bai 1 chuong 9
--x
CREATE INDEX projid_lam_index ON project_lam(projid);


--bai 2 chuong 9
--x
SELECT
    e.empno,
    e.ename,
    e.job,
    m.mrg,
    e.hiredate,
    e.sal,
    e.comm,
    e.deptno
FROM
         emp e
    JOIN emp m ON e.mrg = m.empno
WHERE
    m.mrg = (
        SELECT
            mrg
        FROM
            emp
        WHERE
            ename = '&ename'
    );


--chuong 10

--bai 1 chuong 10
--x
CREATE OR REPLACE VIEW aggredates (
    deptno,
    avgsalary,
    maximun,
    minimun,
    sumsal,
    no_sals,
    no_comms
) AS
    SELECT
        deptno,
        round(AVG(sal), 3),
        MAX(sal),
        MIN(sal),
        SUM(sal),
        COUNT(*),
        SUM(
            CASE
                WHEN comm IS NULL THEN
                    0
                WHEN comm IS NOT NULL THEN
                    1
            END
        )
    FROM
        emp
    GROUP BY
        deptno;
        
        
--bai 2 chuong 10
--x
CREATE VIEW checkviewassignments AS
    SELECT
        *
    FROM
        assignment_lam
    WHERE
            "PROJID" < 2
        AND "A_START_DATE" < "A_END_DATE"
WITH CHECK OPTION;


--bai 3 chuong 10
--x
Create table MESSAGES_LAM ( numcol1 number(9,2),

Numcol2 number(9,2),

Charcol1 varchar2(60),

Charcol2 varchar2(60),

Datecol1 date,

Datecol2 date);




--chuong 12


--bai 1 chuong 12
--x
DECLARE

    CURSOR check_job IS
    SELECT
        job
    FROM
        emp;

    vjob    emp.job%TYPE;
    vcount  NUMBER := 0;

BEGIN

    OPEN check_job;
    LOOP

        FETCH check_job INTO vjob;

        EXIT WHEN check_job%notfound;
        IF vjob = '&job_input' THEN
            vcount := vcount + 1;
        END IF;
    END LOOP;

    IF vcount > 0 THEN

        INSERT INTO messages_lam VALUES (
            0,
            1,
            'FOUNDED ' || vcount,
            'JOB :' || '&job_input',
            sysdate,
            sysdate
        );

    ELSE
        INSERT INTO messages_lam VALUES (
            0,
            0,
            'NOT FOUND ' || vcount,
            'JOB :' || '&job_input',
            sysdate,
            sysdate
        );

    END IF;

    CLOSE check_job;

    COMMIT;
END;