package java_base_1;

public class Bai2 {
  public static void main(String[] args) {
    int sum = 0;
    int count = 0;
    int i = 0;
    while (count < 10) {
      if (i % 2 == 0) {
        sum += i;
        count++;
      }
      i++;
    }

    System.out.println(sum);
  }
}
