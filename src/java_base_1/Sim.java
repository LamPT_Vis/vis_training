package java_base_1;

import java.io.Serializable;

public class Sim implements Serializable {
  public static final long serialVersionUID = 1L;
  private String so;
  private String nhaMang;
  private String soTien;

  public Sim() {}

  public Sim(String so, String nhaMang, String soTien) {
    this.so = so;
    this.nhaMang = nhaMang;
    this.soTien = soTien;
  }

  public String getSo() {
    return so;
  }

  public void setSo(String so) {
    this.so = so;
  }

  public String getNhaMang() {
    return nhaMang;
  }

  public void setNhaMang(String nhaMang) {
    this.nhaMang = nhaMang;
  }

  public String getSoTien() {
    return soTien;
  }

  public void setSoTien(String soTien) {
    this.soTien = soTien;
  }

  @Override
  public String toString() {
    return "So: " + so + ", Nha mang: " + nhaMang + ", So tien: " + soTien + "\n";
  }
}
