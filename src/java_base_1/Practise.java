package java_base_1;

import java.util.Random;

public class Practise {
    public int anInt;

    public Practise(){
        anInt=10;
    }

    public void increatAnInt(){
        synchronized (this){
            this.anInt+=1;
            System.out.println("anInt just has been increated: "+this.anInt);
        }
    }

    public void subAnInt(){
        synchronized (this){
            this.anInt-=1;
            System.out.println("anInt just has been subtracted: "+this.anInt);
        }
    }

    public static void main(String[] args) {
//        for(int i=0;i<20;i++){
//            System.out.println(new Random().ints(5, 21).limit(20).toArray()[new Random().nextInt(20)]);
//        }
        Practise p=new Practise();

        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        p.increatAnInt();
                        Thread.sleep(1000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();
        new Thread(new Runnable() {
            @Override
            public void run() {
                while(true){
                    try {
                        p.subAnInt();
                        Thread.sleep(500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }).start();

    }
}
