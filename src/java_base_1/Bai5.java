package java_base_1;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Bai5 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String s = sc.nextLine();
    s.trim();

    String regex = "^[a-zA-Z][\\w-]+@([\\w]+\\.[\\w]{2,}+|[\\w]+\\.[\\w]{2,}\\.[\\w]{2,})$";
    System.out.println(Pattern.matches(regex, s));
  }
}
