package java_base_1;

import java.util.Scanner;

public class Bai3 {
  public static void main(String[] args) {
    int sum = 2;
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    n -= 1;
    int i = 3;
    while (n > 0) {
      int f = 0;
      for (int j = 2; j < Math.sqrt(i) + 1; j++) {
        if (i % j == 0) {
          f = 0;
          break;
        } else {
          f = 1;
        }
      }
      if (f == 1) {
        System.out.println(i);
        sum += i;
        n--;
      }
      i++;
    }

    System.out.println(sum);
  }
}
