package java_base_1;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.regex.Pattern;

public class Tk2 {

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String search = sc.nextLine();

    String search2 = search.replaceAll("[*]", "[\\\\d]*");

    ArrayList<Sim> queryRes = new ArrayList();
    ArrayList<String> searchList = new ArrayList();
    ArrayList<Sim> result = new ArrayList();
    try {
      BufferedReader re =
          new BufferedReader(new InputStreamReader(new FileInputStream(new File("simso.txt"))));
      DataOutputStream os =
          new DataOutputStream(new FileOutputStream(new File("result.txt"), true));

      String s = re.readLine();
      while (s != null) {
        s.trim();
        String[] ss = s.split(",");
        searchList.add(ss[0]);
        Sim sim = new Sim(ss[0], ss[1], ss[2]);
        queryRes.add(sim);
        s = re.readLine();
      }
      String regex = "^" + search2 + "$";

      for (int i = 0; i < searchList.size(); i++) {
        if (Pattern.matches(regex, searchList.get(i))) {
          System.out.println(queryRes.get(i).toString());
          result.add(queryRes.get(i));
          os.writeChars(queryRes.get(i).toString());
        }
      }

      re.close();
      os.close();

    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
