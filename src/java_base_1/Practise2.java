package java_base_1;

import java.io.*;
import java.util.Scanner;

public class Practise2 {

    public static void main(String[] args) throws IOException {
        System.out.println("nhap vao duong dan file:");
        Scanner scanner = new Scanner(System.in);
        String fileName = scanner.nextLine();
        File file = new File(fileName);
//       FileInputStream fin = null;
//        BufferedInputStream bis = null;
//        fin = new FileInputStream(file);
//        BufferedReader br = new BufferedReader(fin);
        FileReader fr = new FileReader(file);
        BufferedReader br = new BufferedReader(fr);
        FileWriter fw = new FileWriter("write_file.txt");
        String i="";
        while ((i = br.readLine()) != null) {
            if(i.toLowerCase().indexOf("vis training")!=-1){
                System.out.println(i);
                fw.write(i + "\n");
            }
        }
        fw.close();
        br.close();
        fr.close();
    }
}
