package java_base_1;

import java.io.*;
import java.util.ArrayList;
import java.util.Scanner;

public class Bai6 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String path = sc.nextLine();

    File f = new File(path);
    try {
      FileInputStream fis = new FileInputStream(f);
      BufferedReader re = new BufferedReader(new InputStreamReader(fis));
      FileOutputStream fos = new FileOutputStream(new File("output.txt"));
      DataOutputStream os = new DataOutputStream(fos);

      ArrayList<String> arrS = new ArrayList();
      String s = re.readLine();
      while (s != null) {
        System.out.println(s);
        arrS.add(s);
        s = re.readLine();
      }

      String match = "vis training";
      for (int i = 0; i < arrS.size(); i++) {
        if (arrS.get(i).toLowerCase().indexOf(match) != -1) {
          os.writeChars(arrS.get(i) + "\n");
        }
      }

      fis.close();
      re.close();
      fos.close();
      os.close();
    } catch (FileNotFoundException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
