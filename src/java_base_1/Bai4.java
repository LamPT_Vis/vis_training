package java_base_1;

import java.util.Scanner;

public class Bai4 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    String s = sc.nextLine();
    s.trim();
    int count = 0;
    char[] c = s.toCharArray();
    for (int i = 0; i < c.length; i++) {
      if (c[i] == 'a') {
        count++;
      }
    }
    System.out.println(count);
  }
}
