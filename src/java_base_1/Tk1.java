package java_base_1;

import java.util.Scanner;

public class Tk1 {
  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);
    int n = sc.nextInt();
    int i = 1;
    for (int j = 0; j < n; j++) {
      if (j < n - 1) {
        for (int k = 0; k < i; k++) {
          if (k == 0 || k == j) {
            System.out.print("* ");
          } else {
            System.out.print("  ");
          }
        }
        System.out.println();
      } else {
        for (int k = 0; k < i; k++) {
          System.out.print("* ");
        }
      }

      i++;
    }
  }
}
