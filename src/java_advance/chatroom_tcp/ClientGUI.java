package java_advance.chatroom_tcp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientGUI extends javax.swing.JFrame implements Runnable {

  public ClientGUI() throws IOException {
    initComponents();
    isAccessed = 0;
    this.setVisible(true);
    txaAllMess.append("Send any word to start!");
    connectTCP();
  }

  @SuppressWarnings("unchecked")
  private void initComponents() {

    jScrollPane1 = new javax.swing.JScrollPane();
    txaAllMess = new javax.swing.JTextArea();
    tfEditor = new javax.swing.JTextField();
    btnSend = new javax.swing.JButton();
    jLabel2 = new javax.swing.JLabel();

    setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
    setTitle("Client interface");

    txaAllMess.setColumns(20);
    txaAllMess.setRows(20);
    txaAllMess.setEditable(false);
    jScrollPane1.setViewportView(txaAllMess);

    btnSend.setText("Send");
    btnSend.addActionListener(
        new java.awt.event.ActionListener() {
          public void actionPerformed(java.awt.event.ActionEvent evt) {
            btnSendActionPerformed(evt);
          }
        });
    jLabel2.setFont(new java.awt.Font("Dialog", 1, 18)); // NOI18N
    jLabel2.setForeground(new java.awt.Color(255, 0, 0));
    jLabel2.setText("CHATROOM");

    javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
    getContentPane().setLayout(layout);
    layout.setHorizontalGroup(
        layout
            .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(
                layout
                    .createSequentialGroup()
                    .addContainerGap()
                    .addGroup(
                        layout
                            .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(
                                layout
                                    .createSequentialGroup()
                                    .addGroup(
                                        layout
                                            .createParallelGroup(
                                                javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jScrollPane1)
                                            .addGroup(
                                                layout
                                                    .createSequentialGroup()
                                                    .addComponent(tfEditor)
                                                    .addGap(18, 18, 18)
                                                    .addComponent(btnSend)))
                                    .addContainerGap())
                            .addGroup(
                                layout
                                    .createSequentialGroup()
                                    .addComponent(jLabel2)
                                    .addPreferredGap(
                                        javax.swing.LayoutStyle.ComponentPlacement.RELATED,
                                        387,
                                        Short.MAX_VALUE)))));
    layout.setVerticalGroup(
        layout
            .createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(
                layout
                    .createSequentialGroup()
                    .addGap(27, 27, 27)
                    .addGroup(
                        layout
                            .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel2))
                    .addGap(18, 18, 18)
                    .addComponent(
                        jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 297, Short.MAX_VALUE)
                    .addGap(18, 18, 18)
                    .addGroup(
                        layout
                            .createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(
                                tfEditor,
                                javax.swing.GroupLayout.PREFERRED_SIZE,
                                javax.swing.GroupLayout.DEFAULT_SIZE,
                                javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btnSend))
                    .addGap(18, 18, 18)));

    pack();
  }

  private void btnSendActionPerformed(java.awt.event.ActionEvent evt) {
    if (tfEditor.getText().isEmpty()) return;
    try {
      outToServer.writeBytes(tfEditor.getText() + '\n');
      txaAllMess.append("\n[Me]: " + tfEditor.getText());
      outToServer.flush();
      tfEditor.setText("");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void connectTCP() throws IOException {
    clientSocket = new Socket(SERVER_HOST, SERVER_PORT);
    inFromServer = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
    outToServer = new DataOutputStream(clientSocket.getOutputStream());
  }

  private javax.swing.JButton btnSend;
  private javax.swing.JLabel jLabel2;
  private javax.swing.JScrollPane jScrollPane1;
  private javax.swing.JTextField tfEditor;
  private javax.swing.JTextArea txaAllMess;
  private static final String SERVER_HOST = "localhost";
  private static final int SERVER_PORT = 6666;

  private BufferedReader inFromServer;
  private DataOutputStream outToServer;
  private Socket clientSocket;
  private int isAccessed;

  public void ping() throws IOException {
    outToServer.writeBytes("ping" + '\n');
    outToServer.flush();
  }

  @Override
  public void run() {
    try {
      while (true) {
        if (isAccessed == 1) {
          ping();
        }

        String recieve = inFromServer.readLine().trim();
        if (!recieve.equals("null")) {
          txaAllMess.append("\n" + recieve);
        }

        if (recieve.equals("[server]:Access granted")) {
          isAccessed = 1;
          txaAllMess.append("\n------------Start chatting-------------");
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
