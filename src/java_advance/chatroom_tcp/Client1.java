package java_advance.chatroom_tcp;

import java.io.IOException;

public class Client1 {
  public static void main(String args[]) {
    java.awt.EventQueue.invokeLater(
        new Runnable() {
          public void run() {
            try {
              ClientGUI cgui = new ClientGUI();
              Thread thread = new Thread(cgui);
              thread.start();
            } catch (IOException e) {
              e.printStackTrace();
            }
          }
        });
  }
}
