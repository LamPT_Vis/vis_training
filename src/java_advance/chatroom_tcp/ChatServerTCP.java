package java_advance.chatroom_tcp;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class ChatServerTCP {
  private static final int PORT = 6666;
  private ServerSocket serverSocket;

  public ChatServerTCP() throws IOException {
    serverSocket = new ServerSocket(PORT);
  }

  public static void main(String[] args) {
    ExecutorService executor =
        Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());

    try {
      ChatServerTCP server = new ChatServerTCP();
      while (true) {
        Socket client = server.serverSocket.accept();
        System.out.println("connected to " + client.toString());

        executor.execute(new WorkerThread(client));
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
