package java_advance.chatroom_tcp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.regex.Pattern;

public class WorkerThread implements Runnable {

  private Socket socket;
  private String id;
  private BufferedReader clientStream;
  private DataOutputStream serverStream;

  public WorkerThread(Socket socket) throws IOException {
    this.socket = socket;
    this.clientStream = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    this.serverStream = new DataOutputStream(socket.getOutputStream());
  }

  public String getId() {
    return id;
  }

  @Override
  public void run() {

    try {
      String user = clientStream.readLine().trim();
      if (!user.equals("null")) {
        System.out.println("checking in: " + socket.toString());
        serverStream.writeBytes("[server]:Vui long nhap username:" + '\n');
        serverStream.flush();
        user = clientStream.readLine().trim();
        while (checkUsername(user) == false) {
          serverStream.writeBytes("[server]:Username khong dung dinh dang hoac trung" + '\n');
          serverStream.flush();
          user = clientStream.readLine().trim();
        }
        serverStream.writeBytes("[server]:Access granted" + '\n');
        serverStream.flush();
      }

      id = user;
      BroadcastServer.addListUser(id);
      BroadcastServer.handlerRegistry(id, this);

      while (true) {
        String broadcastMes = clientStream.readLine().trim();
        if (!broadcastMes.equals("ping")) {
          System.out.println(broadcastMes);
          BroadcastServer.broadcast(id, broadcastMes);
        }
      }

    } catch (IOException e) {
      System.out.println("Disconnected to " + socket.toString() + " as " + id);
    }
  }

  public boolean checkUsername(String s) {
    String regex = "^(\\w)+$";
    if (!Pattern.matches(regex, s) || BroadcastServer.getListUsername().indexOf(s) != -1) {
      return false;
    } else {
      return true;
    }
  }

  public void sendToClient(String mess) {
    try {
      System.out.println(mess);
      serverStream.writeBytes(mess + '\n');
      serverStream.flush();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
