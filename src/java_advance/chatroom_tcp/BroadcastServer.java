package java_advance.chatroom_tcp;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class BroadcastServer {

  private static List<String> listUsername = new ArrayList<>();
  private static Map<String, WorkerThread> handlerMap = new HashMap<>();

  public static void addListUser(String username) {
    listUsername.add(username);
  }

  public static void handlerRegistry(String username, WorkerThread w) {
    handlerMap.put(username, w);
  }

  public static List<String> getListUsername() {
    return listUsername;
  }

  public static Map<String, WorkerThread> getHandlerMap() {
    return handlerMap;
  }

  public static void broadcast(String user, String incomingMes) {
    for (WorkerThread w : handlerMap.values()) {
      if (!w.getId().equals(user)) {
        handlerMap.get(w.getId()).sendToClient("[" + user + "]: " + incomingMes);
      }
    }
  }
}
