package java_advance.other_op;

import java.util.Random;

public class MultiThread {

  private int randomInt;

  public MultiThread() {
    randomInt = 0;
  }

  public void setRandomInt(int random) { // can be synchronized
    this.randomInt = random;
  }

  public int getRandomInt() { // can be synchronized
    return randomInt;
  }

  public static void main(String[] args) {

    MultiThread m = new MultiThread();
    int end = 0;
    while (end != 10) {
      new Thread(
              new Runnable() {
                @Override
                public void run() {
                  try {
                    Thread.sleep(2000);
                    m.setRandomInt(new Random().nextInt(20));
                    System.out.println("thread 1 random int: " + m.getRandomInt());
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                }
              })
          .run();

      new Thread(
              new Runnable() {
                @Override
                public void run() {
                  try {
                    Thread.sleep(1000);
                    System.out.println(
                        "thread 2 squared thread 1 random int: "
                            + (int) Math.pow(m.getRandomInt(), 2));
                  } catch (InterruptedException e) {
                    e.printStackTrace();
                  }
                }
              })
          .run();
      end++;
    }
  }
}
