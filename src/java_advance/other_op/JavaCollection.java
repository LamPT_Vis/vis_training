package java_advance.other_op;

import java.util.*;

public class JavaCollection {

  public static void main(String[] args) {
    // ---------------vector-------------------

    // default vector with capacity = 10 and element type = Object

//    Vector v0 = new Vector();
//    v0.add(1213); // add a integer
//    v0.add("1213"); // add a String

    // vector with given capacity =20, element type = Integer, auto increase size
//    Vector<Integer> v1 = new Vector<>(20);
//    v1.add(1);
//    v1.add(2);
//    v1.add(1234567);
    // v1.add("sda"); is a syntax error.

    // vector with given capacity =5, type=Integer, increase size = 3 when add 6th element and every
    // time surpass the limit
//    Vector<Integer> v2 = new Vector<>(5, 3);
//    for (int i = 0; i < 6; i++) {
//      v2.add(i);
//    }
//    System.out.println(v2.size()); // return 6
//    System.out.println(v2.capacity()); // return 8

    // Vector ~ ArrayList but is Synchronized -> safe for Threading,however it's recommend to use
    // ArrayList in non-thread environment

    // ------------ArrayList----------
    // like vector but is non-synchronize -> faster , not safe for Threading.

    // ------------Set----------------
    // Set is a interface, a set cannot store duplicated elements and it's no-order.

//    Set<String> setString = new HashSet<>(); // HashSet is a instant
//
//    setString.add("eeee");
//    setString.add("ffff");
//    setString.add("rrrr");
//    setString.add("eeee");
//
//    System.out.println(setString);
//
//    setString.remove("ffff");
//    System.out.println(setString);

    // set has 2 child interfaces, SortedSet and NavigableSet
    // exp: sortedset:
//    SortedSet<String> ts =
//        new TreeSet<String>(); // treeset is a instant, it implements NavigableSet which implements
    // SortedSet.

//    ts.add("India");
//    ts.add("Australia");
//    ts.add("South Africa");
//    ts.add("India");
//
//    System.out.println(ts);
//
//    ts.remove("Australia");
//    System.out.println("Set after removing " + "Australia:" + ts);
//
//    System.out.println("Iterating over set:");
//    // ex iterator
//    Iterator<String> i = ts.iterator();
//    while (i.hasNext()) System.out.println(i.next());
//
//    // ex: NavigableSet:
//    NavigableSet<Integer> ns = new TreeSet<>();
//    ns.add(0);
//    ns.add(1);
//    ns.add(2);
//    ns.add(3);
//    ns.add(4);
//    ns.add(5);
//    ns.add(6);

    // Get a reverse view of the navigable set
//    NavigableSet<Integer> reverseNs = ns.descendingSet();

    // Print the normal and reverse views
//    System.out.println("Normal order: " + ns);
//    System.out.println("Reverse order: " + reverseNs);

    // ---------------Iterator-----------------
    // iterator is a object use to circle through collections as Set, ArrayList,...
    // ex above.

    // ---------------HashMap------------------
    // store data in (key,value) format
    // key/value can in different type.

    HashMap<String, String> capitalCities = new HashMap<>();

    capitalCities.put("England", "London");
    capitalCities.put("Germany", "Berlin");
    capitalCities.put("Norway", "Oslo");
    capitalCities.put("USA", "Washington DC");
    System.out.println(capitalCities);
    System.out.println("======================================");

    Queue<Integer> s=new LinkedList();
    for(int j=0;j<10;j++){
      s.add(j);
    }
    int b=s.poll();
    Iterator<Integer> ite=s.iterator();
//    int a=s.peek();
//    System.out.println("a="+a);
//    while(ite.hasNext()){
//      System.out.println(ite.next());
//    }
    System.out.println("b="+b);
    while(ite.hasNext()){
      System.out.println(ite.next());
    }

    System.out.println("======================================");
    for (String ss : capitalCities.keySet()) {
      System.out.println("key: " + ss + " value: " + capitalCities.get(ss));
    }
  }
}
