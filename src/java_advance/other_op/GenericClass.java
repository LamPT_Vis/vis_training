package java_advance.other_op;

import java.util.ArrayList;

public class GenericClass<T> {
  // T - type
  // T can be used anywhere in this class, represent a type (non-primitive)

  private ArrayList<T> arr;

  public GenericClass() {
    arr = new ArrayList<>();
  }

  public void printGeneric() {
    System.out.println(arr);
  }

  public static void main(String[] args) {
    GenericClass<Integer> gI = new GenericClass<>();
    for (int i = 0; i < 5; i++) {
      gI.arr.add(1000 + i);
    }
    gI.printGeneric();

    GenericClass<String> gS = new GenericClass<>();
    for (int i = 0; i < 5; i++) {
      gS.arr.add("string " + i);
    }
    gS.printGeneric();
  }
}
