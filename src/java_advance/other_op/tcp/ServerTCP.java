package java_advance.other_op.tcp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class ServerTCP {

  private static final int PORT = 4567;

  public static void main(String[] args) throws IOException {
    String stringReplyToClient;

    ServerSocket serverSocket = new ServerSocket(PORT);

    while (true) {
      Socket clientSocket = serverSocket.accept();

      BufferedReader clientStream =
          new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
      DataOutputStream serverStream = new DataOutputStream(clientSocket.getOutputStream());

      System.out.println(clientStream.readLine());

      stringReplyToClient = "OK. Da nhan duoc";

      serverStream.writeBytes(stringReplyToClient + '\n');
      serverStream.flush();
    }
  }
}
