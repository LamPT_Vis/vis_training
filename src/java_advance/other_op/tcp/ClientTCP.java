package java_advance.other_op.tcp;

import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ClientTCP {

  private static final String HOST = "localhost";
  private static final int PORT = 4567;

  public static void main(String[] args) throws IOException {
    while (true) {
      Socket clientSocket = new Socket(HOST, PORT);

      BufferedReader inFromClient = new BufferedReader(new InputStreamReader(System.in));
      BufferedReader inFromServer =
          new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
      DataOutputStream outToServer = new DataOutputStream(clientSocket.getOutputStream());

      System.out.println("input:");
      String s = inFromClient.readLine();

      outToServer.writeBytes(s + '\n');

      System.out.println(inFromServer.readLine());
    }
  }
}
