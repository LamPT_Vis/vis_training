package java_advance.controller;

import java_advance.model.ThucPham;
import java.util.ArrayList;
import java.util.List;

public class ThucPhamController implements HangHoaController {

  private List<ThucPham> arrThucPham;

  public ThucPhamController() {
    arrThucPham = new ArrayList<>();
    ThucPham t1 =
        new ThucPham(
            1,
            "thit bo",
            12,
            130000,
            DateConverter.toDate("2021-01-25"),
            DateConverter.toDate("2021-01-27"),
            "meat deli");
    ThucPham t2 =
        new ThucPham(
            2,
            "thit lon",
            2,
            100000,
            DateConverter.toDate("2021-01-25"),
            DateConverter.toDate("2021-01-26"),
            "meat deli");
    ThucPham t3 =
        new ThucPham(
            3,
            "bap cai",
            10,
            6000,
            DateConverter.toDate("2021-01-23"),
            DateConverter.toDate("2021-01-24"),
            "farm");
    ThucPham t4 =
        new ThucPham(
            4,
            "dua chuot",
            0,
            20000,
            DateConverter.toDate("2021-01-23"),
            DateConverter.toDate("2021-01-26"),
            "farm");
    ThucPham t5 =
        new ThucPham(
            5,
            "rau muong",
            0,
            10000,
            DateConverter.toDate("2021-01-25"),
            DateConverter.toDate("2021-01-27"),
            "farm");
    arrThucPham.add(t1);
    arrThucPham.add(t2);
    arrThucPham.add(t3);
    arrThucPham.add(t4);
    arrThucPham.add(t5);
  }

  @Override
  public List<ThucPham> getAll() {
    return this.arrThucPham;
  }

  @Override
  public void showAll() {
    for (int i = 0; i < arrThucPham.size(); i++) {
      System.out.println(arrThucPham.get(i).toString());
    }
  }

  public void addA(ThucPham a) {
    arrThucPham.add(a);
  }
}
