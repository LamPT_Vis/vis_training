package java_advance.controller;

import java_advance.model.SanhSu;

import java.util.ArrayList;

public class SanhSuController implements HangHoaController {

  private ArrayList<SanhSu> arrSanhSu;

  public SanhSuController() {
    arrSanhSu = new ArrayList();

    SanhSu s1 =
        new SanhSu(12, "bon rua mat", 51, 2000000, "inux", DateConverter.toDate("2021-01-15"));
    SanhSu s2 =
        new SanhSu(13, "bon tam", 20, 4000000, "dolphin", DateConverter.toDate("2021-01-20"));
    SanhSu s3 = new SanhSu(14, "toilet", 10, 2000000, "inux", DateConverter.toDate("2021-01-15"));

    arrSanhSu.add(s1);
    arrSanhSu.add(s2);
    arrSanhSu.add(s3);
  }

  @Override
  public ArrayList<SanhSu> getAll() {
    return this.arrSanhSu;
  }

  @Override
  public void showAll() {
    for (int i = 0; i < arrSanhSu.size(); i++) {
      System.out.println(arrSanhSu.get(i).toString());
    }
  }

  public void addA(SanhSu s) {
    arrSanhSu.add(s);
  }
}
