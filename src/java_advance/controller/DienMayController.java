package java_advance.controller;

import java_advance.model.DienMay;

import java.util.ArrayList;

public class DienMayController implements HangHoaController {

  private ArrayList<DienMay> arrDienMay;

  public DienMayController() {
    arrDienMay = new ArrayList();

    DienMay d1 = new DienMay(6, "may giat", 2, 10000000, 12, 1200);
    DienMay d2 = new DienMay(7, "may rua bat", 10, 5000000, 6, 1500);
    DienMay d3 = new DienMay(8, "lo vi song", 5, 3000000, 6, 3000);
    DienMay d4 = new DienMay(9, "may bom", 7, 1000000, 24, 300);
    DienMay d5 = new DienMay(10, "may hut bui", 1, 3000000, 12, 300);
    DienMay d6 = new DienMay(11, "dieu hoa", 0, 12000000, 12, 12000);

    arrDienMay.add(d1);
    arrDienMay.add(d2);
    arrDienMay.add(d3);
    arrDienMay.add(d4);
    arrDienMay.add(d5);
    arrDienMay.add(d6);
  }

  @Override
  public ArrayList<DienMay> getAll() {
    return this.arrDienMay;
  }

  @Override
  public void showAll() {
    for (int i = 0; i < arrDienMay.size(); i++) {
      System.out.println(arrDienMay.get(i).toString());
    }
  }

  public void addA(DienMay d) {
    arrDienMay.add(d);
  }
}
