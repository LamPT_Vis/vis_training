package java_advance.controller;

import java.text.ParsePosition;
import java.text.SimpleDateFormat;
import java.util.Date;

public class DateConverter {

  private DateConverter() {}

  // convert date to string
  public static String toStringDate(Date date) {
    return new SimpleDateFormat("dd-mm-yyyy").format(date);
  }

  // convert string to date
  public static Date toDate(String sdate) {
    return new SimpleDateFormat("dd-mm-yy").parse(sdate, new ParsePosition(0));
  }
}
