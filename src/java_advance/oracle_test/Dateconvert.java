package java_advance.oracle_test;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Dateconvert {
  public Dateconvert(){

  }

  // convert date to string
  public static String toStringDate(Date date) {
    return new SimpleDateFormat("dd-mm-yyyy").format(date);
  }
}
