package java_advance.oracle_test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class DAO {
  public static void main(String[] args) {
    try {
      Class.forName("oracle.jdbc.driver.OracleDriver");

      Connection con =
          DriverManager.getConnection("jdbc:oracle:thin:@localhost:1521:xe", "system", "oracle");

      Statement stm = con.createStatement();
      ResultSet res = stm.executeQuery("select * from emp");

      while (res.next()) {
        System.out.println(
            res.getInt(1)
                + " "
                + res.getString(2)
                + " "
                + res.getString(3)
                + " "
                + res.getString(4)
                + " "
                + Dateconvert.toStringDate(res.getDate(5)));
      }
    } catch (ClassNotFoundException e) {
      e.printStackTrace();
    } catch (SQLException e) {
      e.printStackTrace();
    }
  }
}
