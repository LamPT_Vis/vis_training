package java_advance.model;

public class DienMay extends HangHoa {

  private int tgBaoHanh;
  private int congSuat;
  private static final double VAT = 0.1;

  // --constructors
  public DienMay() {
    super();
  }

  public DienMay(
      int maHang, String tenHang, int soLuongTon, long donGia, int tgBaoHanh, int congSuat) {
    super(maHang, tenHang, soLuongTon, donGia);
    this.tgBaoHanh = tgBaoHanh;
    this.congSuat = congSuat;
  }

  // --getter, setter

  public int getTgBaoHanh() {
    return tgBaoHanh;
  }

  public void setTgBaoHanh(int tgBaoHanh) {
    this.tgBaoHanh = tgBaoHanh;
  }

  public int getCongSuat() {
    return congSuat;
  }

  public void setCongSuat(int congSuat) {
    this.congSuat = congSuat;
  }

  public double getVAT() {
    return VAT;
  }

  // --tostring
  public String toString() {
    return "ma hang: "
        + maHang
        + ", ten hang: "
        + tenHang
        + ", so luong ton: "
        + soLuongTon
        + ", don gia: "
        + donGia
        + ", thoi gian bao hanh: "
        + tgBaoHanh
        + ", cong suat: "
        + congSuat
        + ", VAT: "
        + VAT;
  }
}
