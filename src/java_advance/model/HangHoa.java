package java_advance.model;

public class HangHoa {
  protected int maHang;
  protected String tenHang;
  protected int soLuongTon;
  protected long donGia;

  // --constructor
  public HangHoa() {}

  public HangHoa(int maHang, String tenHang, int soLuongTon, long donGia) {
    this.maHang = maHang;
    this.tenHang = tenHang;
    this.soLuongTon = soLuongTon;
    this.donGia = donGia;
  }

  // --getter, setter
  public int getMaHang() {
    return maHang;
  }

  public String getTenHang() {
    return tenHang;
  }

  public int getSoLuongTon() {
    return soLuongTon;
  }

  public long getDonGia() {
    return donGia;
  }

  public void setTenHang(String tenHang) {
    this.tenHang = tenHang;
  }

  public void setSoLuongTon(int soLuongTon) {
    this.soLuongTon = soLuongTon;
  }

  public void setDonGia(long donGia) {
    this.donGia = donGia;
  }
}
