package java_advance.model;

import java_advance.controller.DateConverter;

import java.util.Date;

public class ThucPham extends HangHoa {

  private Date ngaySX;
  private Date ngayHH;
  private String nhaCC;
  private static final double VAT = 0.05;

  // --constructor
  public ThucPham() {
    super();
  }

  public ThucPham(
      int maHang,
      String tenHang,
      int soLuongTon,
      long donGia,
      Date ngaySX,
      Date ngayHH,
      String nhaCC) {
    super(maHang, tenHang, soLuongTon, donGia);
    this.ngaySX = ngaySX;
    this.ngayHH = ngayHH;
    this.nhaCC = nhaCC;
  }

  // --getter, setter
  public Date getNgaySX() {
    return ngaySX;
  }

  public void setNgaySX(Date ngaySX) {
    this.ngaySX = ngaySX;
  }

  public Date getNgayHH() {
    return ngayHH;
  }

  public void setNgayHH(Date ngayHH) {
    this.ngayHH = ngayHH;
  }

  public String getNhaCC() {
    return nhaCC;
  }

  public void setNhaCC(String nhaCC) {
    this.nhaCC = nhaCC;
  }

  public double getVAT() {
    return VAT;
  }

  // --tostring
  @Override
  public String toString() {
    return "maHang: "
        + maHang
        + ", tenHang: "
        + tenHang
        + ", soLuongTon: "
        + soLuongTon
        + ", donGia: "
        + donGia
        + ", ngaySX: "
        + DateConverter.toStringDate(ngaySX)
        + ", ngayHH: "
        + DateConverter.toStringDate(ngayHH)
        + ", nhaCC: "
        + nhaCC
        + ", VAT: "
        + VAT;
  }
}
