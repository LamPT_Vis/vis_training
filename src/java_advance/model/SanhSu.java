package java_advance.model;

import java_advance.controller.DateConverter;

import java.util.Date;

public class SanhSu extends HangHoa {

  private String nhaSX;
  private Date ngayNhap;
  private static final double VAT = 0.1;

  // --constructors
  public SanhSu() {
    super();
  }

  public SanhSu(
      int maHang, String tenHang, int soLuongTon, long donGia, String nhaSX, Date ngayNhap) {
    super(maHang, tenHang, soLuongTon, donGia);
    this.nhaSX = nhaSX;
    this.ngayNhap = ngayNhap;
  }

  // --getter, setter

  public String getNhaSX() {
    return nhaSX;
  }

  public void setNhaSX(String nhaSX) {
    this.nhaSX = nhaSX;
  }

  public Date getNgayNhap() {
    return ngayNhap;
  }

  public void setNgayNhap(Date ngayNhap) {
    this.ngayNhap = ngayNhap;
  }

  public double getVAT() {
    return VAT;
  }

  // --tostring
  public String toString() {
    return "ma hang: "
        + maHang
        + ", ten hang: "
        + tenHang
        + ", so luong ton: "
        + soLuongTon
        + ", don gia: "
        + donGia
        + ", nha san xuat: "
        + nhaSX
        + ", ngay nhap: "
        + DateConverter.toStringDate(ngayNhap)
        + ", VAT: "
        + VAT;
  }
}
