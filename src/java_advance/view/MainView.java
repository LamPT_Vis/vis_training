package java_advance.view;

import java_advance.controller.DateConverter;
import java_advance.controller.DienMayController;
import java_advance.controller.SanhSuController;
import java_advance.controller.ThucPhamController;
import java_advance.model.DienMay;
import java_advance.model.SanhSu;
import java_advance.model.ThucPham;

import java.util.ArrayList;
import java.util.Date;
import java.util.Scanner;

public class MainView {

  private ThucPhamController tpc;
  private DienMayController dmc;
  private SanhSuController ssc;
  private ArrayList<Integer> allId;

  public MainView() {
    tpc = new ThucPhamController();
    dmc = new DienMayController();
    ssc = new SanhSuController();

    allId = new ArrayList();
  }

  public static void main(String[] args) {
    Scanner sc = new Scanner(System.in);

    MainView mv = new MainView();

    for (int i = 0; i < mv.tpc.getAll().size(); i++) {
      mv.allId.add(mv.tpc.getAll().get(i).getMaHang());
    }
    for (int i = 0; i < mv.dmc.getAll().size(); i++) {
      mv.allId.add(mv.dmc.getAll().get(i).getMaHang());
    }
    for (int i = 0; i < mv.ssc.getAll().size(); i++) {
      mv.allId.add(mv.ssc.getAll().get(i).getMaHang());
    }
    int exit = 1;
    do {
      System.out.println(
          "---------------Menu----------------\n"
              + "1. Xem danh sach hang hoa\n"
              + "2. Them hang hoa\n"
              + "3. Xem danh gia tung mat hang\n"
              + "--------Chon:");

      int a = sc.nextInt();

      switch (a) {
        case 1:
          {
            int exit2 = 1;
            do {
              System.out.println(
                  "-----------Chon loai hang hoa-----------\n"
                      + "1. Thuc pham.\n"
                      + "2. Dien may\n"
                      + "3. Sanh su\n"
                      + "4. Tat ca.\n"
                      + "-----------Chon:");
              int b = sc.nextInt();

              switch (b) {
                case 1:
                  mv.tpc.showAll();
                  break;

                case 2:
                  mv.dmc.showAll();
                  break;

                case 3:
                  mv.ssc.showAll();
                  break;

                case 4:
                  mv.tpc.showAll();
                  mv.dmc.showAll();
                  mv.ssc.showAll();
                  break;

                default:
                  exit2 = 0;
                  break;
              }
            } while (exit2 == 1);
            break;
          }
        case 2:
          {
            int exit2 = 1;
            do {
              System.out.println(
                  "-----------Chon loai hang hoa-----------\n"
                      + "1. Thuc pham.\n"
                      + "2. Dien may\n"
                      + "3. Sanh su\n"
                      + "-----------Chon:");
              int b = sc.nextInt();

              switch (b) {
                case 1:
                  {
                    System.out.println("Nhap chi tiet cho thuc pham:\n" + "Ma hang:");
                    try {
                      int maHang;
                      do {
                        maHang = sc.nextInt();
                      } while (mv.allId.contains(maHang));
                      System.out.println("Ten hang:");
                      sc.nextLine();
                      String tenHang = sc.nextLine();
                      System.out.println("So luong ton:");
                      int soLuongTon = sc.nextInt();
                      System.out.println("Don gia:");
                      long donGia = sc.nextLong();
                      System.out.println("ngay san xuat:");
                      sc.nextLine();
                      String ngaySX = sc.nextLine();
                      System.out.println("ngay het han:");
                      String ngayHH = sc.nextLine();
                      System.out.println("nha cung cap:");
                      String nhaCC = sc.nextLine();

                      ThucPham t =
                          new ThucPham(
                              maHang,
                              tenHang,
                              soLuongTon,
                              donGia,
                              DateConverter.toDate(ngaySX),
                              DateConverter.toDate(ngayHH),
                              nhaCC);

                      mv.tpc.addA(t);
                      System.out.println("them thanh cong:");
                      mv.tpc.showAll();
                    } catch (Exception e) {
                      System.out.println(
                          "them khong thanh cong do loi, quay lai chon loai hang hoa");
                    }
                    break;
                  }
                case 2:
                  {
                    System.out.println("Nhap chi tiet cho san pham dien may:\n" + "Ma hang:");
                    try {
                      int maHang;
                      do {
                        maHang = sc.nextInt();
                      } while (mv.allId.contains(maHang));
                      System.out.println("Ten hang:");
                      sc.nextLine();
                      String tenHang = sc.nextLine();
                      System.out.println("So luong ton:");
                      int soLuongTon = sc.nextInt();
                      System.out.println("Don gia:");
                      long donGia = sc.nextLong();
                      System.out.println("thoi gian bao hanh:");
                      int tgBaoHanh = sc.nextInt();
                      System.out.println("Cong suat:");
                      int congSuat = sc.nextInt();

                      DienMay d =
                          new DienMay(maHang, tenHang, soLuongTon, donGia, tgBaoHanh, congSuat);

                      mv.dmc.addA(d);
                      System.out.println("them thanh cong:");
                      mv.dmc.showAll();
                    } catch (Exception e) {
                      System.out.println(
                          "them khong thanh cong do loi, quay lai chon loai hang hoa");
                    }
                    break;
                  }
                case 3:
                  {
                    System.out.println("Nhap chi tiet cho san pham sanh su:\n" + "Ma hang:");
                    try {
                      int maHang;
                      do {
                        maHang = sc.nextInt();
                      } while (mv.allId.contains(maHang));
                      System.out.println("Ten hang:");
                      sc.nextLine();
                      String tenHang = sc.nextLine();
                      System.out.println("So luong ton:");
                      int soLuongTon = sc.nextInt();
                      System.out.println("Don gia:");
                      long donGia = sc.nextLong();
                      System.out.println("nha san xuat:");
                      sc.nextLine();
                      String nhaSX = sc.nextLine();
                      System.out.println("ngay nhap:");
                      String ngayNhap = sc.nextLine();

                      SanhSu s =
                          new SanhSu(
                              maHang,
                              tenHang,
                              soLuongTon,
                              donGia,
                              nhaSX,
                              DateConverter.toDate(ngayNhap));
                      mv.ssc.addA(s);
                      System.out.println("them thanh cong:");
                      mv.ssc.showAll();
                    } catch (Exception e) {
                      System.out.println(
                          "them khong thanh cong do loi, quay lai chon loai hang hoa");
                    }
                    break;
                  }
                default:
                  {
                    exit2 = 0;
                    break;
                  }
              }
            } while (exit2 == 1);
            break;
          }
        case 3:
          {
            int exit2 = 1;
            do {
              System.out.println(
                  "-----------Chon loai hang hoa-----------\n"
                      + "1. Thuc pham.\n"
                      + "2. Dien may\n"
                      + "3. Sanh su\n"
                      + "-----------Chon:");
              int b = sc.nextInt();

              switch (b) {
                case 1:
                  {
                    Date now = DateConverter.toDate("2021-01-26");

                    for (ThucPham t : mv.tpc.getAll()) {
                      if (t.getSoLuongTon() > 0 && t.getNgayHH().before(now)) {
                        System.out.println(t.getTenHang() + " kho ban");
                      } else {
                        System.out.println(t.getTenHang() + " khong danh gia");
                      }
                    }

                    break;
                  }
                case 2:
                  {
                    for (DienMay d : mv.dmc.getAll()) {
                      if (d.getSoLuongTon() < 3) {
                        System.out.println(d.getTenHang() + " ban chay");
                      } else {
                        System.out.println(d.getTenHang() + " khong danh gia");
                      }
                    }
                    break;
                  }
                case 3:
                  {
                    Date now = DateConverter.toDate("2021-01-26");
                    for (SanhSu s : mv.ssc.getAll()) {
                      long nodate =
                          (now.getTime() - s.getNgayNhap().getTime()) / (24 * 3600 * 1000);
                      if (nodate > 10 && s.getSoLuongTon() > 50) {
                        System.out.println(s.getTenHang() + " ban cham");
                      } else {
                        System.out.println(s.getTenHang() + " khong danh gia");
                      }
                    }
                    break;
                  }
                default:
                  {
                    exit2 = 0;
                    break;
                  }
              }
            } while (exit2 == 1);

            break;
          }
        default:
          {
            exit = 0;
            break;
          }
      }
    } while (exit == 1);
  }
}
